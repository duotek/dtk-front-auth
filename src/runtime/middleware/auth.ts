import { useAuthStore } from "../stores/useAuthStore.js";
import {
	navigateTo,
	defineNuxtRouteMiddleware,
	useRuntimeConfig,
} from "#imports";
export default defineNuxtRouteMiddleware((to) => {
	const authStore = useAuthStore();
	const config = useRuntimeConfig().public.auth;
	if (!authStore.token && config.redirects.guest !== false && to.path !== config.redirects.guest) {
	  return navigateTo(config.redirects.guest + (config.redirects.external ? '?action=auth-redirect' : ''), { replace: true, external: config.redirects.external });
	}
});
