import { useAuthStore } from '../stores/useAuthStore'
import {
  navigateTo,
  defineNuxtRouteMiddleware,
  useRuntimeConfig,
} from '#imports'

export default defineNuxtRouteMiddleware((to) => {
  const authStore = useAuthStore()
  const config = useRuntimeConfig().public.auth

  if (authStore.token && config.redirects.auth !== false && to.path !== config.redirects.auth) {
    return navigateTo(config.redirects.auth + (config.redirects.external ? '?action=guest-redirect' : ''), { replace: true, external: config.redirects.external });
  }
})
