import { computed } from 'vue'
import { $fetch } from 'ofetch'
import { createPinia } from 'pinia'
import { useAuthStore } from './stores/useAuthStore'
import {
  defineNuxtPlugin,
  useRuntimeConfig,
  useCookie,
  navigateTo,
} from '#app'

export default defineNuxtPlugin(async (nuxtApp) => {
  const config = useRuntimeConfig().public.auth

  if (!config || !config.cookie || !config.strategies) {
    return
  }

  if (!nuxtApp.$pinia) {
    const pinia = createPinia()
    nuxtApp.vueApp.use(pinia)
  }

  const baseApiUrl = useRuntimeConfig().public.app_env.BASE_API_URL
  const authStore = useAuthStore()

  const tokenCookieName = `${config.cookie.prefix}token`
  const strategyCookieName = `${config.cookie.prefix}strategy`

  const expireDate = new Date()
  expireDate.setTime(expireDate.getTime() + (config.cookie.options.expires * 24 * 60 * 60 * 1000))

  const tokenCookie = useCookie(tokenCookieName, { expires: expireDate })
  const strategyCookie = useCookie(strategyCookieName, { expires: expireDate })

  const setToken = (token: string, strategy: string) => {
    tokenCookie.value = token
    authStore.setToken(token)
    strategyCookie.value = strategy
  }

  const setUser = (user: any) => {
    authStore.setUser(user)
  }

  const handleAuthError = (error: any) => {
    tokenCookie.value = null
    strategyCookie.value = null

    authStore.clearAuth()

    if (config.redirects.guest) {
      navigateTo(config.redirects.guest, { replace: true, external: config.redirects.external })
    }

    console.error('Authentication error:', error)
  }

  const fetchUser = async (strategy: string = 'signIn') => {
    const userEndpoint = config.strategies[strategy].endpoints.user

    if (!userEndpoint) return

    try {
      let user = await $fetch(`${baseApiUrl}${userEndpoint.url}`, {
        method: userEndpoint.method,
        headers: {
          Authorization: `${config.strategies[strategy].tokenType} ${authStore.token}`,
          Accept: 'application/json, text/plain, */*',
        },
      })
      if (userEndpoint.propertyName && user[userEndpoint.propertyName]) user = user[userEndpoint.propertyName]
      setUser(user)
    }
    catch (error) {
      handleAuthError(error)
    }
  }

  const initializeAuth = async () => {
    const token = tokenCookie.value
    const strategy = strategyCookie.value || 'signIn'

    if (token && !authStore.token) {
      authStore.setToken(token)
      if (!authStore.user) {
        await fetchUser(strategy)
      }
    }
  }

  const signIn = async (credentials: any) => {
    return await authenticateUser('signIn', credentials)
  }

  const signUp = async (credentials: any) => {
    return await authenticateUser('signUp', credentials)
  }

  const authenticateUser = async (strategy: string, credentials: any) => {
    const loginEndpoint = config.strategies[strategy].endpoints.login

    try {
      const data = await $fetch(`${baseApiUrl}${loginEndpoint.url}`, {
        method: loginEndpoint.method,
        body: credentials,
      })
      setToken(data[loginEndpoint.propertyName || 'token'], strategy)
      await fetchUser(strategy)

      if (config.redirects.auth) {
        navigateTo(config.redirects.auth, { replace: true })
      }

      return data
    }
    catch (error) {
      console.error(`${strategy === 'signIn' ? 'Login' : 'Registration'} error:`, error)
      throw error
    }
  }

  const logout = async () => {
    const strategy = strategyCookie.value || 'signIn'
    const logoutEndpoint = config.strategies[strategy].endpoints.logout

    try {
      await $fetch(`${baseApiUrl}${logoutEndpoint.url}`, {
        method: logoutEndpoint.method,
        headers: {
          Authorization: `${config.strategies[strategy].tokenType} ${authStore.token}`,
          Accept: 'application/json, text/plain, */*',
        },
      })

      tokenCookie.value = null
      strategyCookie.value = null

      authStore.clearAuth()

      if (config.redirects.guest) {
        navigateTo(config.redirects.guest, { replace: true, external: config.redirects.external })
      }
    }
    catch (error) {
      console.error('Logout error:', error)
    }
  }

  const getToken = () => authStore.token

  const user = computed(() => authStore.user)
  const isLoggedIn = computed(() => !!authStore.token)

  const authFetch = $fetch.create({
    baseURL: baseApiUrl,
    onRequest({ options }) {
      options.headers = options.headers ? Object.fromEntries(Object.entries(options.headers)) : {}
      if (authStore.token) {
        options.headers = options.headers || {}
        options.headers['Authorization'] = `${config.strategies[strategyCookie.value || 'signIn'].tokenType} ${authStore.token}`
      }
      options.headers['Accept'] = 'application/json, text/plain, */*'
    },
    onResponseError({ response }) {
      if (response.status === 401 && isLoggedIn.value) {
        tokenCookie.value = null
        strategyCookie.value = null

        authStore.clearAuth()

        if (config.redirects.guest) {
          navigateTo(
            config.redirects.guest + (config.redirects.external ? '?action=auth-redirect' : ''),
            { replace: true, external: config.redirects.external },
          )
        }
        else if (config.redirects.logout) {
          navigateTo(
            config.redirects.logout,
            { replace: true, external: config.redirects.external },
          )
        }
      }
    },
  })

  nuxtApp.provide('authFetch', authFetch)

  nuxtApp.provide('auth', {
    setToken,
    setUser,
    fetchUser,
    initializeAuth,
    signIn,
    signUp,
    logout,
    getToken,
    get user() {
      return user.value
    },
    get isLoggedIn() {
      return isLoggedIn.value
    },
  })

  await initializeAuth()
})
