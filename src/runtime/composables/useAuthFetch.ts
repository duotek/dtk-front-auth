import { useAuthStore } from '../stores/useAuthStore'
import { useFetch, useRuntimeConfig } from '#imports'

export const useAuthFetch = async (url: string, options: any = {}) => {
  const authStore = useAuthStore()
  const token = authStore.token

  if (!options.headers) {
    options.headers = {}
  }

  if (token) {
    options.headers['Authorization'] = `Bearer ${token}`
  }

  const config = useRuntimeConfig()

  return await useFetch(`${config.public.app_env.BASE_API_URL}${url}`, {
    ...options,
    headers: {
      ...options.headers,
    },
  })
}
