import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
  state: () => ({
    user: null,
    token: null,
  }),
  getters: {
    isLoggedIn: state => !!state.user && !!state.token,
  },
  actions: {
    setUser(user: any) {
      this.user = user
    },
    setToken(token: any) {
      this.token = token
    },
    clearAuth() {
      this.user = null
      this.token = null
    },
  },
})
