import {
  defineNuxtModule,
  addPlugin,
  createResolver,
  addImports,
  addRouteMiddleware,
  installModule,
} from '@nuxt/kit'

export interface ModuleOptions {
  redirects: {
    auth: any
    guest: any
  }
  cookie: {
    prefix: string
    options: {
      expires: number
    }
  }
  strategies: {
    signUp?: StrategyOptions
    signIn?: StrategyOptions
  }
}

export interface StrategyOptions {
  tokenType: string
  endpoints: {
    login: EndpointOptions
    logout: EndpointOptions
    user: EndpointOptions
  }
}

export interface EndpointOptions {
  url: string
  method: string
  propertyName?: string
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'duotek-nuxt3-auth',
    configKey: 'auth',
  },
  async setup(options, nuxt) {
    await installModule('@pinia/nuxt')

    nuxt.options.runtimeConfig.public.auth = options

    const { resolve } = createResolver(import.meta.url)

    addPlugin(resolve('./runtime/plugin'))
    addImports([
      {
        name: 'useAuthFetch',
        as: 'useAuthFetch',
        from: resolve('./runtime/composables/useAuthFetch'),
      },
      {
        name: 'useAuthStore',
        as: 'useAuthStore',
        from: resolve('./runtime/stores/auth'),
      },
    ])
    addRouteMiddleware({
      name: 'auth',
      path: resolve('./runtime/middleware/auth'),
      global: true,
    })
    addRouteMiddleware({
      name: 'guest',
      path: resolve('./runtime/middleware/guest'),
      global: true,
    })
  },
})
