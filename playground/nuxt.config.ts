export default defineNuxtConfig({
  modules: ['../src/module'],
  devtools: { enabled: true },
  auth: {
    redirects: {
      auth: '/',
      guest: '/login',
    },
    cookie: {
      prefix: 'tnh-terminal.',
      options: {
        expires: 300,
      },
    },
    strategies: {
      signUp: {
        tokenType: 'Bearer',
        endpoints: {
          login: {
            url: '/customers/self/register',
            method: 'post',
            propertyName: 'token',
          },
          logout: {
            url: '/customers/self/logout',
            method: 'post',
          },
          user: {
            url: '/customers/self/detail',
            method: 'get',
          },
        },
      },
      signIn: {
        tokenType: 'Bearer',
        endpoints: {
          login: {
            url: '/customers/self/auth',
            method: 'post',
            propertyName: 'token',
          },
          logout: {
            url: '/customers/self/logout',
            method: 'post',
          },
          user: {
            url: '/customers/self/detail',
            method: 'get',
          },
        },
      },
    },
  },
  runtimeConfig: {
    public: {
      app_env: {
        BASE_API_URL: 'https://api.example.com',
      },
      auth: {
        redirects: {
          auth: '/',
          guest: '/login',
        },
        cookie: {
          prefix: 'exmpl.',
          options: {
            expires: 300,
          },
        },
        strategies: {
          signUp: {
            tokenType: 'Bearer',
            endpoints: {
              login: {
                url: '/customers/self/register',
                method: 'post',
                propertyName: 'token',
              },
              logout: {
                url: '/customers/self/logout',
                method: 'post',
              },
              user: {
                url: '/customers/self/detail',
                method: 'get',
              },
            },
          },
          signIn: {
            tokenType: 'Bearer',
            endpoints: {
              login: {
                url: '/customers/self/auth',
                method: 'post',
                propertyName: 'token',
              },
              logout: {
                url: '/customers/self/logout',
                method: 'post',
              },
              user: {
                url: '/customers/self/detail',
                method: 'get',
              },
            },
          },
        },
      },
    },
  },
})
